class EconetUnknownId(Exception):
    pass


class EconetUninitilized(Exception):
    pass


class EconetUnauthorized(Exception):
    pass


class EconetHTTPException(Exception):
    pass
