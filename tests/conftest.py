import pytest_asyncio
from enum import Enum


@pytest_asyncio.fixture
async def rm_params_units_names(aioresponses):
    payload = {"data": ["", "°C", "sek.", "min.", "h.", "%", "kg", "kW", "obr/min"]}
    aioresponses.get("http://localhost/econet/rmParamsUnitsNames", payload=payload)
    yield payload


@pytest_asyncio.fixture
async def rm_alarms_names(aioresponses):
    payload = {
        "data": {
            "11": "Broken fan",
            "19": "Broken fuel servo",
            "18": "Open STB.",
            "1": "Broken boiler thermostat",
            "0": "No power",
            "3": "Broken feeder thermostat",
            "2": "Furnace temerature to high!",
            "5": "Broken fumes sensor",
            "4": "To high feeder temperature",
            "7": "Unable to light up",
            "255": "Alert ongoing!",
        }
    }
    aioresponses.get("http://localhost/econet/rmAlarmsNames", payload=payload)
    yield payload


@pytest_asyncio.fixture
async def rm_params_enums(aioresponses):
    payload = {
        "data": [
            {"values": [], "first": 0},
            {"values": ["OFF", "ON"], "first": 0},
            {"values": ["STOP", "START", "", "Kalibracja"], "first": 0},
        ]
    }
    aioresponses.get("http://localhost/econet/rmParamsEnums", payload=payload)
    yield payload


@pytest_asyncio.fixture
async def rm_current_data_params(aioresponses):
    payload = {
        "data": {
            "1280": {"name": "Test PRESET_BOILER_TEMPERATURE", "special": 1, "unit": 1},
            "1792": {"name": "Test OPERATION_MODE", "special": 2, "unit": 31},
        }
    }
    aioresponses.get("http://localhost/econet/rmCurrentDataParams", payload=payload)
    yield payload


@pytest_asyncio.fixture
async def rm_params_data(aioresponses):
    payload = {
        "data": [
            {
                "value": 20,
                "maxv": 25,
                "minv": 15,
                "edit": True,
                "unit": 1,
                "mult": 1.0,
                "offset": 0,
            },
        ]
    }
    aioresponses.get(
        "http://localhost/econet/rmParamsData", payload=payload, repeat=True
    )
    yield payload


@pytest_asyncio.fixture
async def sys_params(aioresponses):
    payload = {
        "uid": "uid",
        "ecosrvSoftVer": "1.0",
        "modulePanelSoftVer": "2.0",
        "moduleASoftVer": "3.0",
        "controllerID": "test_controller",
        "settingsVer": "4.0",
    }
    aioresponses.get("http://localhost/econet/sysParams", payload=payload)
    yield payload


@pytest_asyncio.fixture
async def reg_params_data(aioresponses):
    payload = {"data": {"1280": 10, "1792": 1}}
    aioresponses.get(
        "http://localhost/econet/regParamsData", payload=payload, repeat=True
    )
    yield payload


@pytest_asyncio.fixture
async def econet(mocker):
    mocker.patch("plum_econet.Econet.set_param")

    from plum_econet import Econet

    e = Econet("http://localhost", "user", "pass")
    await e.setup()
    yield e


@pytest_asyncio.fixture
async def default_econet(
    rm_params_enums,
    rm_params_units_names,
    rm_alarms_names,
    rm_current_data_params,
    rm_params_data,
    reg_params_data,
    sys_params,
    econet,
):
    yield econet
