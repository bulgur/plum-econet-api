# CHANGELOG


## v0.13.0 (2025-01-21)

### Features

- Don't add enum for exception params
  ([`7aa761a`](https://gitlab.com/bulgur/plum-econet/-/commit/7aa761a0f69122f1ddfbac4897a39f3070d268f2))


## v0.12.0 (2024-10-09)

### Features

- Lowercase all enums
  ([`b8e0451`](https://gitlab.com/bulgur/plum-econet/-/commit/b8e0451dcd7a4375b434a1624889f47e6ea8e0de))


## v0.11.1 (2024-10-09)

### Bug Fixes

- Convert _value to int, rather then return pure _value
  ([`41a4390`](https://gitlab.com/bulgur/plum-econet/-/commit/41a43908dca0649d41bda6d46716d7c34105ee5a))


## v0.11.0 (2024-10-09)

### Features

- Access enum only if _value is type int
  ([`a81a683`](https://gitlab.com/bulgur/plum-econet/-/commit/a81a68368acf307aca10417b935ba29a8f2ada7e))


## v0.10.0 (2024-09-27)

### Bug Fixes

- Use float type for setting temperature and param values
  ([`09d1f51`](https://gitlab.com/bulgur/plum-econet/-/commit/09d1f51ca2efe5f65633846aa09e94c04b7df684))

### Features

- Add burner_output sensor
  ([`4a745bb`](https://gitlab.com/bulgur/plum-econet/-/commit/4a745bb52ce540524c8257b2769d3066359f7de9))

- Add summer_mode option to Smartfire object
  ([`9733784`](https://gitlab.com/bulgur/plum-econet/-/commit/9733784f2885f4c7ec6a6c111b290aab9bd0ce4c))

- Rename parameter to BLOW_IN_OUTPUT_50
  ([`a9c0f07`](https://gitlab.com/bulgur/plum-econet/-/commit/a9c0f0771639087a3e70d09b1a8f5382cbbcd998))

### Refactoring

- Cleanup adding sensors and settings to Smartfire
  ([`530cab4`](https://gitlab.com/bulgur/plum-econet/-/commit/530cab4097677dfc976dece2c4756ecdd0ad4fb6))


## v0.9.3 (2024-09-25)

### Bug Fixes

- Bump version
  ([`db202b8`](https://gitlab.com/bulgur/plum-econet/-/commit/db202b8febadab7695c22f041d15bac0b32658aa))


## v0.9.2 (2024-09-25)

### Bug Fixes

- Fix ci upload stage
  ([`68669ef`](https://gitlab.com/bulgur/plum-econet/-/commit/68669ef06fbe0a2f4f154f63b80b7e0ae39ae624))


## v0.9.1 (2024-09-25)

### Bug Fixes

- Bump aiohttp to enable installation on newer home assistant versions
  ([`70b0abe`](https://gitlab.com/bulgur/plum-econet/-/commit/70b0abedc8d49aa30d6e9e112b396ff98adaee13))


## v0.9.0 (2024-02-28)

### Features

- Add raw value getter
  ([`6c21e74`](https://gitlab.com/bulgur/plum-econet/-/commit/6c21e74185086d8e36ca241bcf1d35ae0aadbe57))

### Testing

- Add test for enumerated property
  ([`a539196`](https://gitlab.com/bulgur/plum-econet/-/commit/a539196677a79e3b87d0b38f1689801f80257860))


## v0.8.1 (2024-02-15)

### Bug Fixes

- Add needs configs to some of CI stages
  ([`c761fb8`](https://gitlab.com/bulgur/plum-econet/-/commit/c761fb801c68633496be3605ad7856c30a06bfcf))

- Bump aiohttp version to 3.9.x
  ([`346f28f`](https://gitlab.com/bulgur/plum-econet/-/commit/346f28fd493abdb3fdf56ee4fd2b86d601c56979))

### Continuous Integration

- Add release steps
  ([`8a6dba6`](https://gitlab.com/bulgur/plum-econet/-/commit/8a6dba67e03c6f016024a4fdb3645457d98c0781))

- Call 'version' command when using semantic-release tool
  ([`5cc9aa9`](https://gitlab.com/bulgur/plum-econet/-/commit/5cc9aa9d3bfe980bfccea83912613b5a04e9ece2))

- Check ssh-key
  ([`3f2c438`](https://gitlab.com/bulgur/plum-econet/-/commit/3f2c438f6437e44885ed4b7667591917f649801f))

- Clone repo instead of fetch in CI
  ([`bbc0c3c`](https://gitlab.com/bulgur/plum-econet/-/commit/bbc0c3c24e3a7d9ef98a8c35aab0bc5c40a731ba))

- Configure semantic-release for Gitlab
  ([`248a244`](https://gitlab.com/bulgur/plum-econet/-/commit/248a244d0598fa9fc6bcbf3a7508127419186e9a))

- Copy ssh-key instead of cating it
  ([`d5accc7`](https://gitlab.com/bulgur/plum-econet/-/commit/d5accc76a1f0b198527b9d248f6071feca13b32d))

- Debug ssh access
  ([`57ac8bb`](https://gitlab.com/bulgur/plum-econet/-/commit/57ac8bbd78410af7f574eec88fb8357f4dcaaa32))

- Fix indentation in the release step
  ([`8825fee`](https://gitlab.com/bulgur/plum-econet/-/commit/8825fee5246e10e6e88424745cc9c878b1789c08))

- Fix semantic-release vcs parameter
  ([`6a5ad4d`](https://gitlab.com/bulgur/plum-econet/-/commit/6a5ad4dac3edc16e3a1e9bc61fc52761153b2f22))

- Install git command in python images
  ([`b63fecd`](https://gitlab.com/bulgur/plum-econet/-/commit/b63fecde5d466cfc3364514fb6812763e8ba02e7))

- Install openssh-client
  ([`43244c1`](https://gitlab.com/bulgur/plum-econet/-/commit/43244c1bc415d14f86c072b4235d521594bd1f3c))

- Make semantic-release work on any branch
  ([`4898c6a`](https://gitlab.com/bulgur/plum-econet/-/commit/4898c6a6c549b10dcb40e41b888f8da1a4b09825))

- Push version using ssh
  ([`6deb01d`](https://gitlab.com/bulgur/plum-econet/-/commit/6deb01db2af5986595b7d26f71b340ba7a41ef72))

- Remove 'only' as it colides with 'rules'
  ([`d4e125b`](https://gitlab.com/bulgur/plum-econet/-/commit/d4e125b2fe4cb38fa67bc6c36e6d4858375096f0))

- Set GIT_DEPTH in version step as well
  ([`8660586`](https://gitlab.com/bulgur/plum-econet/-/commit/86605865140ce9f1059a46c39a7d8f7b175d1a10))

- Set GIT_DEPTH to 0
  ([`4b5cd96`](https://gitlab.com/bulgur/plum-econet/-/commit/4b5cd96835978a33be7cd35fa0523e976838e0db))

- Switch git branch with exact name
  ([`a4da52e`](https://gitlab.com/bulgur/plum-econet/-/commit/a4da52e0ac478215bb01f34496bc0f4c13f67d61))

- Switch to a branch from detached state
  ([`cdaffdc`](https://gitlab.com/bulgur/plum-econet/-/commit/cdaffdcea796042c34e06608a3ea27513f81a52d))


## v0.8.0 (2023-09-01)

### Features

- Add fuel level related sensors
  ([`cc414ec`](https://gitlab.com/bulgur/plum-econet/-/commit/cc414eccd0aad85884eec55a38f631274893bfbf))


## v0.7.1 (2023-09-01)

### Bug Fixes

- Properly get the alarm name from dict
  ([`9baad44`](https://gitlab.com/bulgur/plum-econet/-/commit/9baad44236209fff4de3f2aa83ed8f5f3d1be7a3))


## v0.7.0 (2023-09-01)

### Features

- Reorganize alarms and add property to Smartfire to extract them
  ([`b65b913`](https://gitlab.com/bulgur/plum-econet/-/commit/b65b913a1eca311fff664426ab43c3875e799547))


## v0.6.0 (2023-09-01)

### Features

- Added alarms support
  ([`8592e6f`](https://gitlab.com/bulgur/plum-econet/-/commit/8592e6f207d57537f47804e40e3497cf1a6793d8))


## v0.5.0 (2022-10-07)

### Bug Fixes

- Enhance Params list with missing mixer settings
  ([`333c5a5`](https://gitlab.com/bulgur/plum-econet/-/commit/333c5a54848a318b445bf5ed1d24f91103e0e83f))

Use mentioned new Params in the Smartfire.mixers settings, as those will contain minv and maxv
  values.

Changelog: fix

### Features

- Param object does not accept enums list anymore
  ([`d6b3810`](https://gitlab.com/bulgur/plum-econet/-/commit/d6b38100df6829df8ce40641a109bf690b7d2dfb))

breaking_change: Enums list is no longer accepted in the Param.__init__

This is replaced by directly setting single values in the Param.enum if needed, which will be used
  by Param.value getter.

Changelog: breaking_change

### Refactoring

- **api**: All fetch_* now return data instead of updating internal state
  ([`65b41e3`](https://gitlab.com/bulgur/plum-econet/-/commit/65b41e3ff94c1a3ee3f08de31c3eb437891837f1))

Externally nothing changed, setup and update take care of this change, thus get and set params work
  the same as well.

Changelog: refactor


## v0.4.0 (2022-10-05)

### Code Style

- Proper format in conftest.py
  ([`18033a7`](https://gitlab.com/bulgur/plum-econet/-/commit/18033a7fc0aae8141c4008042bec4f9f3baec41a))

### Documentation

- Update README to include removal of Smartfire.central_heating
  ([`e44fc06`](https://gitlab.com/bulgur/plum-econet/-/commit/e44fc0611ba9f5e0700f30e07d8c4b45ab3cae43))

### Features

- Add Mixer object and use it in the Smartfire
  ([`9184df2`](https://gitlab.com/bulgur/plum-econet/-/commit/9184df2dc3be0a94d771b5cbd897d72de5f9f5bc))

breaking_change: Removed properties:

- Smartfire.central_heating - Smartfire.central_heating_pump - Smartfire.mixer1_valve Introduced
  Smartfire.mixers in that place. If Smartfire is configured with at least one mixer, that can be
  used to track temperature set for central_heating. Otherwise Smartfire.boiler should be used for
  this, since most probly there is analog mixer in the central_heating pipeing

Mixer object will include temperature control and also pump and valve sensors

- Econet.setup fetches now params values as well
  ([`e045408`](https://gitlab.com/bulgur/plum-econet/-/commit/e0454088eb46134a04a7c4ce75a4c5369b13970a))

breaking_change: This change is needed because some of informations

needed to properly initialize Param database. Some parameters, like Mixers, are reported initialy,
  however it is possible to know that those are used by the boiler only when we receive data for
  them, since if they are not set up in the boiler, values will be `null`.


## v0.3.1 (2022-10-04)

### Bug Fixes

- Ignore data update for parameters not reported earlier
  ([`a2ad2d3`](https://gitlab.com/bulgur/plum-econet/-/commit/a2ad2d3c525da2bd64b107d5cffe04beb664d1cc))

### Documentation

- Remove setup from README example, as it's not needed there
  ([`0910214`](https://gitlab.com/bulgur/plum-econet/-/commit/0910214e47132ff3f1a954ddf8e0eb85fcaa9043))


## v0.3.0 (2022-10-03)

### Chores

- Add CI
  ([`8a40658`](https://gitlab.com/bulgur/plum-econet/-/commit/8a406580e3b6735eca976d8f02cad80f8a3b1dc0))

- Add semantic_release tool configuration
  ([`ec9e73f`](https://gitlab.com/bulgur/plum-econet/-/commit/ec9e73f4a54c129e70d67162b2b57fdbd6766c76))

- Be more specific in sed statement
  ([`8f62cd2`](https://gitlab.com/bulgur/plum-econet/-/commit/8f62cd20b4860f1c6aa6404075d3e2515db14023))

- Enable only Python 3.10 builds, due to used feature set
  ([`2dff437`](https://gitlab.com/bulgur/plum-econet/-/commit/2dff437aecb4d725b29c08878f40dc4b8577b0a1))

- Fix linting non-Python files
  ([`6f2e140`](https://gitlab.com/bulgur/plum-econet/-/commit/6f2e14002c89f86ad18ceac60a738c0778b11e5d))

- Lower setuptools requirements pre 3.10 Python releases
  ([`3b207ba`](https://gitlab.com/bulgur/plum-econet/-/commit/3b207ba9277ea6bea233fa85fd7b026fc1135e8a))

- Preinstall build package
  ([`acfaa37`](https://gitlab.com/bulgur/plum-econet/-/commit/acfaa37aba8de191dedf002b0eebfc6c359eb308))

### Code Style

- Fix formatting
  ([`f932b1a`](https://gitlab.com/bulgur/plum-econet/-/commit/f932b1a88021bad376830cede441dea2e3f5b99b))

### Features

- Extend Smartfire with pumps sensors
  ([`7591b0c`](https://gitlab.com/bulgur/plum-econet/-/commit/7591b0cc1edafcdca2c7444a6085de0e9d0997b8))

### Testing

- Add missing tests for Smartfire sensors
  ([`51c66d6`](https://gitlab.com/bulgur/plum-econet/-/commit/51c66d6b2abcb94c290e21b64aede1dade2e9ddb))


## v0.2.0 (2022-10-03)

### Chores

- Fix tox with pyproject.toml
  ([`43f451a`](https://gitlab.com/bulgur/plum-econet/-/commit/43f451a0f877c0e5484fc3a05b7e47eddaa7bb4c))

### Features

- In case of not reported parameter from API return "Undefined"
  ([`c5bee51`](https://gitlab.com/bulgur/plum-econet/-/commit/c5bee511eb8e23232cd8420f2013e98d8c8f4901))


## v0.1.1 (2022-10-03)

### Bug Fixes

- Expose underlying econet api from Smartfire object
  ([`608485d`](https://gitlab.com/bulgur/plum-econet/-/commit/608485d7ba01c9e8c50d0bd117a587eda83e9e46))

### Chores

- Use pyproject.toml
  ([`42250c6`](https://gitlab.com/bulgur/plum-econet/-/commit/42250c66d033e240eb8e3fc5c476229e955d568a))


## v0.1.0 (2022-10-03)

### Documentation

- Add LICENSE
  ([`6ca8d2b`](https://gitlab.com/bulgur/plum-econet/-/commit/6ca8d2b2a4dc9caa5b8ad1de44a89845db17091b))

- Update README
  ([`f8bf608`](https://gitlab.com/bulgur/plum-econet/-/commit/f8bf608d66da2320295c773f9029a6cadf367d05))

### Features

- Improve on, added tests
  ([`2382373`](https://gitlab.com/bulgur/plum-econet/-/commit/238237344ecea5062b6841e166726b18dec76ddd))

Added Smartfire and some higher level objects to track and modify underlying Smartfire boiler


## v0.0.1 (2022-10-03)

### Features

- Initial commit
  ([`4ed6d86`](https://gitlab.com/bulgur/plum-econet/-/commit/4ed6d8686291833f8dee0b0453245b92bf5bb03f))
